package com.atlantis.Atlantis;

import com.atlantis.Atlantis.controller.ItemController;

import com.atlantis.Atlantis.model.Item;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ItemControllerTest {

    @Autowired
    private ItemController itemController;

    @Test
    public void testGetItems() throws Exception {
        Optional<Integer> page = Optional.of(0);
        Optional<Integer> size = Optional.of(15);

        assertTrue(itemController.getItems(page,size).getTotalElements() > 0);
    }

    @Test
    public void testGetItemsEmptyParams() throws Exception {
        Optional<Integer> page = Optional.empty();
        Optional<Integer> size = Optional.empty();

        assertTrue(itemController.getItems(page,size).getTotalElements() > 0);
    }

    @Test
    public void testGetItemsBestSellingWeekly() throws Exception {
        assertTrue(itemController.getItemsBestSellingWeekly().size() > 0);
    }
}