package com.atlantis.Atlantis;

import com.atlantis.Atlantis.model.Customer;
import com.atlantis.Atlantis.model.Invoice;
import com.atlantis.Atlantis.model.Line;
import com.atlantis.Atlantis.service.InvoiceService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InvoiceServiceTest {

    @Autowired
    private InvoiceService invoiceService;

    @Test
    public void testSaveInvoice() throws Exception {
        Invoice invoice = new Invoice();
        Customer customer = new Customer();
        Line line = new Line();

        customer.setFirstSurname("Ada");
        customer.setName("Augusta");
        customer.setSecondSurname("Byron");

        line.setCode("0001");
        line.setDescription("Funko pop del muñeco diabólico Chucky");
        line.setName("Funko Chucky");
        line.setPrice(new BigDecimal(3.23));
        line.setQuantity(3d);

        invoice.setDate(new Date());
        invoice.setCustomer(customer);
        invoice.setLines(Arrays.asList(line));

        Assertions.assertTrue(invoiceService.save(invoice));
    }

    @Test
    public void testSaveInvoiceErrorLineQuantity() throws Exception {
        Invoice invoice = new Invoice();
        Customer customer = new Customer();
        Line line = new Line();

        customer.setFirstSurname("Ada");
        customer.setName("Augusta");
        customer.setSecondSurname("Byron");

        line.setCode("0001");
        line.setDescription("Funko pop del muñeco diabólico Chucky");
        line.setName("Funko Chucky");
        line.setPrice(new BigDecimal(3.23));
        line.setQuantity(100d);

        invoice.setDate(new Date());
        invoice.setCustomer(customer);
        invoice.setLines(Arrays.asList(line));

        Assertions.assertFalse(invoiceService.save(invoice));
    }
}