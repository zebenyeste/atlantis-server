INSERT INTO item (active, code, description, insertdate, name, stock)
    VALUES
    (true, '0001', 'Funko pop del muñeco diabólico Chucky', '2023-02-23', 'Funko Chucky', 23),
    (true, '0002', 'Funko pop de marven Hulk', '2023-02-23', 'Funko Hulk', 12),
    (true, '0003', 'Funko pop de la NBA Michael Jordan', '2023-02-23', 'Funko Michael Jordan', 9),
    (true, '0004', 'Funko pop de piloto Fernando Alonso', '2023-02-23', 'Funko Fernando Alonso', 4),
    (true, '0005', 'Comic One Piece 1 - Edición limitada', '2023-02-23', 'One piece comic 1', 3);

INSERT INTO price (startdate, enddate, price, item_id)
    VALUES
    ('2023-02-01', '2023-02-10', 3.23, 1),
    ('2023-02-11', '2023-02-20', 2.99, 1),
    ('2023-02-21', '2023-02-28', 2.95, 1),
    ('2023-02-01', '2023-02-10', 0.99, 2),
    ('2023-02-11', '2023-02-20', 0.98, 2),
    ('2023-02-21', '2023-02-28', 0.97, 2),
    ('2023-02-01', '2023-02-10', 1.21, 3),
    ('2023-02-11', '2023-02-20', 1.22, 3),
    ('2023-02-21', '2023-02-28', 1.23, 3),
    ('2023-02-01', '2023-02-10', 9.89, 4),
    ('2023-02-01', '2023-02-10', 12.01, 5);

INSERT INTO customer (name, firstsurname, secondsurname)
    VALUES
    ('Pedro', 'Borges', 'Rodríguez'),
    ('Pablo', 'Ruiz', 'Picasso'),
    ('Carlos', 'Ruiz', 'Zafón');

INSERT INTO invoice (customer_id, date)
    VALUES
    (1, '2023-02-02'),
    (1, '2023-02-03'),
    (2, '2023-02-05');

INSERT INTO line (code, description, invoice_id, name, price, quantity)
    VALUES
    ('0001', 'Funko pop del muñeco diabólico Chucky', 1, 'Funko Chucky', 3.23, 1),
    ('0002', 'Funko pop de marven Hulk', 1, 'Funko Hulk', 0.99, 3),
    ('0003', 'Funko pop de la NBA Michael Jordan', 1, 'Funko Michael Jordan', 1.21, 1),
    ('0003', 'Funko pop de la NBA Michael Jordan', 2, 'Funko Michael Jordan', 1.21, 1),
    ('0004', 'Funko pop de piloto Fernando Alonso', 2, 'Funko Fernando Alonso', 9.89, 2);
