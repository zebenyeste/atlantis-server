package com.atlantis.Atlantis.utils;

import com.atlantis.Atlantis.model.Item;
import com.atlantis.Atlantis.model.Line;
import com.atlantis.Atlantis.repository.ItemRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InvoiceUtils {

    public InvoiceUtils() {
    }

    public boolean isValidLines(List<Line> lines, ItemRepository itemRepository){
        boolean isValid = true;
        List<String> codes = new ArrayList<String>();
        List<Item> itemList = new ArrayList<Item>();
        HashMap<String, Double> linesHashMap = new HashMap<String, Double>();

        for (Line line: lines) {
            if(line.getQuantity() <= 0 || line.getCode().isEmpty() || line.getDescription().isEmpty() ||
                    line.getName().isEmpty() || BigDecimal.ZERO.compareTo(line.getPrice()) >= 0){
                isValid = false;
                break;
            } else {
                codes.add(line.getCode());
            }
        }

        // For each line to recover that stock is available
        if(isValid){
            itemList = itemRepository.findByCodeIn(codes);

            groupStockByCode(linesHashMap, lines);

            for (Item item : itemList){
                if(linesHashMap.containsKey(item.getCode())){
                    if(item.getStock() < linesHashMap.get(item.getCode())){
                        isValid = false;
                        break;
                    }
                }
            }
        }

        return isValid;
    }

    public HashMap<String, Double> groupStockByCode(HashMap<String, Double> stockHashMap, List<Line> lines){
        lines.forEach(line -> {
            Double quantity = (stockHashMap.containsKey(line.getCode())) ? stockHashMap.get(line.getCode()): 0;
            stockHashMap.put(line.getCode(), quantity + line.getQuantity());
        });

        return stockHashMap;
    }
}
