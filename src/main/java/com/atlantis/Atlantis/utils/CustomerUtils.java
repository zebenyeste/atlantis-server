package com.atlantis.Atlantis.utils;

import com.atlantis.Atlantis.model.Customer;

public class CustomerUtils {

    public CustomerUtils() {
    }

    public boolean isValidCustomer(Customer customer){
        boolean isValid = true;

        if(customer.getName().isEmpty() || customer.getFirstSurname().isEmpty() || customer.getSecondSurname().isEmpty()){
            isValid = false;
        }
        return isValid;
    }
}
