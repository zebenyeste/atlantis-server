package com.atlantis.Atlantis.controller;

import com.atlantis.Atlantis.model.Item;
import com.atlantis.Atlantis.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class ItemController {

    final int DEFAULT_ITEM_PAGE = 0;
    final int DEFAULT_ITEM_SIZE = 10;
    final int DEFAULT_DAYS = 7;
    final int DEFAULT_MAX_ITEM_SELLING = 3;

    @Autowired
    ItemService itemService;

    @GetMapping("/items")
    public Page<Item> getItems(@RequestParam Optional<Integer> page, @RequestParam Optional<Integer> size) {

        int pageParam = (page.isPresent() && page.get() > 0) ? page.get() : DEFAULT_ITEM_PAGE;
        int sizeParam = (size.isPresent() && size.get() > 0) ? size.get() : DEFAULT_ITEM_SIZE;

        return itemService.getItems(pageParam, sizeParam);
    }

    @GetMapping("/items/best-selling-weekly")
    public List<Item> getItemsBestSellingWeekly() {
        return itemService.getItemsBestSelling(DEFAULT_DAYS, DEFAULT_MAX_ITEM_SELLING);
    }
}
