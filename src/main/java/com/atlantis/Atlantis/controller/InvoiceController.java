package com.atlantis.Atlantis.controller;

import com.atlantis.Atlantis.model.Invoice;

import com.atlantis.Atlantis.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class InvoiceController {

    @Autowired
    InvoiceService invoiceService;

    @PostMapping("/invoice")
    @ResponseStatus(HttpStatus.CREATED)
    public void save(@RequestBody Invoice invoice) throws Exception {
        if(!invoiceService.save(invoice)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
