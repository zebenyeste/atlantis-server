package com.atlantis.Atlantis.repository;

import com.atlantis.Atlantis.model.Item;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemPageableRepository extends PagingAndSortingRepository<Item, Integer> {

    public List<Item> findAll();

}