package com.atlantis.Atlantis.repository;

import com.atlantis.Atlantis.model.Invoice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface InvoiceRepository extends CrudRepository<Invoice, Integer> {

    public List<Invoice> findByDateBetween(Date start, Date end);

}