package com.atlantis.Atlantis.repository;

import com.atlantis.Atlantis.model.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ItemRepository extends CrudRepository<Item, Integer> {

    public List<Item> findByCodeIn(List<String> codes);

}