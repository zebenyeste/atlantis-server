package com.atlantis.Atlantis.service;

import com.atlantis.Atlantis.model.Invoice;
import com.atlantis.Atlantis.model.Item;
import com.atlantis.Atlantis.repository.InvoiceRepository;
import com.atlantis.Atlantis.repository.ItemPageableRepository;
import com.atlantis.Atlantis.repository.ItemRepository;
import com.atlantis.Atlantis.utils.InvoiceUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ItemService {

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    ItemPageableRepository itemPageableRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    InvoiceUtils invoiceUtils = new InvoiceUtils();

    public List<Item> getItems() {
        return StreamSupport.stream(itemRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    /**
     * This function returns the items per page and quantity on that page.
     * @param page page where the listing begins
     * @param size total items to be recovered
     * @return the list of best-selling items
     */
    public Page<Item> getItems(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return itemPageableRepository.findAll(pageable);
    }

    /**
     * This function retrieves the most best-selling items in a period of time from the current date to the days passed
     * by parameter. It returns a maximum number of items according to the maxItemSelling parameter. If any of the
     * parameters is less than or equal to zero, an empty list is returned.
     * @param days number of days backward to search for best-selling items
     * @param maxItemSelling maximum number of items to be recovered
     * @return the list of best-selling items
     */
    public List<Item> getItemsBestSelling(int days, int maxItemSelling) {
        List<Invoice> invoiceList = new ArrayList<>();
        List<String> itemCodeList = new ArrayList<String>();
        List<Item> itemFilterList = new ArrayList<>();
        HashMap<String, Double> linesHashMap = new HashMap<String, Double>();
        LocalDate localDate = LocalDate.now().minusDays(days);
        Date startDate = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Map<String, Double> sortedMap;

        if(days > 0 && maxItemSelling > 0){

            // Retrieve invoices from x days ago and group the lines by code adding their amount
            invoiceList = invoiceRepository.findByDateBetween(startDate, new Date());

            invoiceList.forEach(invoice -> {
                invoiceUtils.groupStockByCode(linesHashMap, invoice.getLines());
            });

            // Sort rows by quantity from highest to lowest
            sortedMap =
                linesHashMap.entrySet().stream()
                    .sorted(Map.Entry.<String, Double> comparingByValue().reversed())
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));

            itemCodeList.addAll(sortedMap.keySet());

            // Retrieve items and sort by quantity
            List<Item> itemList = itemRepository.findByCodeIn(itemCodeList);
            List<Item> itemFilterListAux = new ArrayList<>();
            itemCodeList.forEach(code -> {

                Optional<Item> itemOptional = Optional.ofNullable(itemList.stream()
                        .filter(item -> item.getCode().equalsIgnoreCase(code))
                        .findAny()
                        .orElse(null));

                if(itemOptional.isPresent()){
                    itemFilterListAux.add(itemOptional.get());
                }
            });

            itemFilterList = itemFilterListAux;

            if(itemFilterListAux.size() > 0 && maxItemSelling < itemFilterListAux.size()){
                itemFilterList = itemFilterListAux.subList(0, maxItemSelling);
            }
        }

        return itemFilterList;
    }
}
