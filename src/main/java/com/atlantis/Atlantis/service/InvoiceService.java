package com.atlantis.Atlantis.service;

import com.atlantis.Atlantis.model.Invoice;
import com.atlantis.Atlantis.model.Item;
import com.atlantis.Atlantis.repository.InvoiceRepository;
import com.atlantis.Atlantis.repository.ItemRepository;
import com.atlantis.Atlantis.utils.CustomerUtils;
import com.atlantis.Atlantis.utils.InvoiceUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class InvoiceService {

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    ItemRepository itemRepository;

    CustomerUtils customerUtils = new CustomerUtils();
    InvoiceUtils invoiceUtils = new InvoiceUtils();

    public boolean save(Invoice invoice) {

        boolean isValidInvoice = true;

        // Check customer data
        isValidInvoice = customerUtils.isValidCustomer(invoice.getCustomer());

        // Check invoice line data
        if(isValidInvoice){
            isValidInvoice = invoiceUtils.isValidLines(invoice.getLines(), itemRepository);

            if(isValidInvoice){

                // Update stock items and then save invoice
                HashMap<String, Double> linesHashMap = new HashMap<String, Double>();
                List<Item> itemList = new ArrayList<Item>();

                invoiceUtils.groupStockByCode(linesHashMap, invoice.getLines());
                itemList = itemRepository.findByCodeIn(new ArrayList<String>(linesHashMap.keySet()));

                itemList.forEach(item -> {
                    item.setStock(item.getStock() - linesHashMap.get(item.getCode()));
                });

                itemRepository.saveAll(itemList);
                invoiceRepository.save(invoice);
            }
        }

        return isValidInvoice;
    }
}
